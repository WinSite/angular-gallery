(function() {
    'use strict';

    var gallery = angular.module('ngMyGallery', []);

    gallery.directive('myGallery', ['$interval', '$http', 'filterFilter', '$filter', function($interval, $http, filterFilter, $filter) {
        var rangeOfInt = function(int) {
            var tempArr = [],
                range = int;
            for (var i = 0; i <= range; i++) {
                tempArr.push(i);
            }
            return tempArr;
        };
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                feed: '=',
                search: '=',
                pagination: '=',
                resultsPerPage: '=',
                sorting: '=',
                autoRotateTime: '='
            },
            link: function(scope, elem, attrs) {
                //Initialise scope
                scope.filteredImages = [],
                    scope.images = [],
                    scope.error = false,
                    scope.configuration = {
                        feed: scope.feed,
                        search: typeof scope.search == 'undefined' ? true : scope.search,
                        pagination: typeof scope.pagination == 'undefined' ? true : scope.pagination,
                        resultsPerPage: typeof scope.resultsPerPage == 'undefined' ? 10 : scope.resultsPerPage,
                        sorting: typeof scope.sorting == 'undefined' ? true : scope.sorting,
                        sortingMethod: '',
                        autoRotateTime: typeof scope.autoRotateTime == 'undefined' ? 4 : scope.autoRotateTime,
                        searchText: '',
                        slideShow: false
                    };

                //Pagination scope object
                scope.paginationController = {
                    currentPage: 0,
                    prevPage: function() {
                        if (this.currentPage > 0) {
                            this.setPage(this.currentPage - 1);
                        }
                    },
                    pageCount: function() {
                        return Math.ceil(scope.filteredImages.length / scope.configuration.resultsPerPage) - 1;
                    },
                    prevPageDisabled: function() {
                        return this.currentPage === 0 ? true : false;
                    },
                    range: function() {
                        return rangeOfInt(this.pageCount());
                    },
                    setPage: function(n) {
                        scope.slideShowController.currentSlide = 0;
                        scope.slideShowController.initInterval();
                        this.currentPage = n;
                    },
                    nextPage: function() {
                        if (this.currentPage < this.pageCount()) {
                            this.setPage(this.currentPage + 1);
                        }
                    },
                    nextPageDisabled: function() {
                        return this.currentPage === this.pageCount() ? true : false;
                    },
                    imagesInCurrentPage: function() {
                        var slidesInPage = scope.configuration.resultsPerPage - 1;
                        if (scope.paginationController.pageCount() == scope.paginationController.currentPage && scope.filteredImages.length % scope.configuration.resultsPerPage !== 0) {
                            slidesInPage = (scope.filteredImages.length % scope.configuration.resultsPerPage) - 1;
                        }
                        return slidesInPage;
                    },
                };

                //SlideShow scope object
                scope.slideShowController = {
                    currentSlide: 0,
                    nextSlide: function() {
                        if (this.currentSlide == scope.paginationController.imagesInCurrentPage()) {
                            this.currentSlide = 0;
                            return;
                        }
                        this.currentSlide++;
                    },
                    range: function() {
                        return rangeOfInt(scope.paginationController.imagesInCurrentPage());
                    },
                    setSlide: function(n) {
                        this.currentSlide = n;
                    },
                    slideInterval: undefined,
                    initInterval: function() {
                        if (this.slideInterval !== undefined) {
                            $interval.cancel(this.slideInterval);
                            this.slideInterval = undefined;
                        }
                        this.slideInterval = $interval(function() {
                            scope.slideShowController.nextSlide();
                        }, scope.configuration.autoRotateTime * 1000);
                    },
                    toggleSlideShow: function() {
                        if (!scope.configuration.slideShow) {
                            scope.configuration.slideShow = true;
                            this.initInterval();
                        }
                        else {
                            scope.configuration.slideShow = false;
                        }
                    },
                };

                //Modal scope object
                scope.modalController = {
                    modalOpen: false,
                    toggleModal: function(n) {
                        if (scope.configuration.slideShow === true)
                            return;
                        this.modalIndex = n;
                        this.modalOpen = true;
                    },
                    closeModal: function() {
                        this.modalOpen = false;
                    },
                    modalNext: function() {
                        if (this.modalIndex == scope.paginationController.imagesInCurrentPage()) {
                            this.modalIndex = 0;
                            return;
                        }
                        this.modalIndex++;
                    },
                    modalPrev: function() {
                        if (this.modalIndex === 0) {
                            this.modalIndex = scope.paginationController.imagesInCurrentPage();
                            return;
                        }
                        this.modalIndex--;
                    }
                };

                //Reset pagination & slideshow modules
                var resetPaginagtionAndSlideShow = function() {
                    scope.paginationController.setPage(0);
                    if (typeof scope.slideShowController !== 'undefined')
                        scope.slideShowController.currentSlide = 0;
                        scope.slideShowController.initInterval();

                };

                //initialise images feed
                var initFeed = function(data) {
                    if(data == 'error') return scope.error = true;
                    for (var i = 0; i < data.length; i++) {
                        if(!data[i].date || !data[i].title || !data[i].url) return scope.error = true;
                        data[i].date = new Date(data[i].date);
                    }
                    scope.images = data;
                    scope.filteredImages = scope.images;
                    scope.$watch('configuration.searchText', function(newValue) {
                        console.log(newValue);
                        scope.filteredImages = filterFilter(scope.images, newValue);
                        resetPaginagtionAndSlideShow();
                    });
                    scope.$watch("configuration.resultsPerPage", function(newValue) {
                        resetPaginagtionAndSlideShow();
                    });
                };
                
                
                var getFeed = function(feedUrl) {
                    if (typeof feedUrl === 'object')
                        return initFeed(feedUrl);
                    $http.get(feedUrl)
                        .success(function(data, status, headers, config) {
                            return initFeed(data);
                        })
                        .error(function(data, status, headers, config) {
                            return initFeed('error');
                        });
                };

                getFeed(scope.configuration.feed);
            },
            templateUrl: 'templates/gallery.html'
        };
    }]);

    gallery.filter('offset', function() {
        return function(input, start) {
            start = parseInt(start, 10);
            if (!input) return;
            return input.slice(start);
        };
    });

    gallery.directive('imageload', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var errSrc = 'http://dummyimage.com/800x600&text=Sorry, image is not available!';
                element.bind('load', function() {
                    element.css('opacity', '1');
                    angular.element(element.parent().children()[5]).css('opacity', 0);
                });
                element.bind('error', function() {
                    if (attrs.src != errSrc) {
                        attrs.$set('src', errSrc);
                    }
                });
            }
        };
    });
}())